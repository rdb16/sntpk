<?php

/*
* Created on 13 juil. 2015
* By RDB@sntpk.com
*
*/
include ("includes/tophtml.php");
include ("includes/header.php");
include("includes/menu.php");
echo "<div id='corps'> <h2>Je rentre dans testCode.php </h2><br />";
include_once ("includes/functions.php");
echo "<h2>j'ai inclu les fonctions </h2><br /><br />";
$myConf = readConf("config/squelette.conf");
echo $strerreur;
var_dump($myConf);

/*
 * Maintenant on teste la base de données
 */

echo '<br /> <br />';
echo 'addressBase vaut : ' . $myConf['addressBase'];
echo '<br /> <br />';
echo ' <h2>Test de la connexion a la base </h2><br /><br />';
try {
	$connStr = 'mysql:host='.$myConf['addressBase'].';dbname='.$myConf['nameBase'];
	$arrExtraParam = array (
		PDO :: MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
	);
	$pdo = new PDO($connStr, $myConf['utLogin'], $myConf['utPass'], $arrExtraParam);
	$pdo->setAttribute(PDO :: ATTR_ERRMODE, PDO :: ERRMODE_EXCEPTION);

	//pour notre test:
	$query = 'SELECT *' . ' FROM users' . ' WHERE 1' . ' LIMIT ?;';
	$prep = $pdo->prepare($query);
	$prep->bindValue(1, 4, PDO :: PARAM_INT);

	//Compiler et exécuter la requête
	$prep->execute();

	//Récupérer toutes les données retournées
	$arrAll = $prep->fetchAll(PDO :: FETCH_ASSOC); // ASSOC évite de doubler avec un index entier

	//affichage
	foreach ($arrAll as $row) {
		foreach ($row as $cle => $valeur) {
			echo "$cle = ";
			echo "$valeur ; ";
		}
		echo "<br />";
	}
	//Clore la requête préparée
	$prep->closeCursor();
	$prep = NULL;
	//clore la liaison
	$pdo = null;

} catch (PDOException $e) {
	$msg = 'ERREUR PDO dans ' . $e->getFile() . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
	die($msg);
}
echo '<br /> <br />';
echo '</div>';

include ("includes/footer.php");
?>
