
 <div id="corps" >
       <section> 
		<h3>Vous êtes sur la page d'accueil</h3>
       	<p>
        Votre entreprise évolue dans un contexte compétitif et vous souhaitez offrir à vos clients plus de services.<br />
        Notre société accompagne les directions générales et informatiques de PME/PMI dans le cadre de leurs projets et fait du
         conseil informatique.
         Notre équipe, constituée de consultants pluri disciplinaires, développe à la fois une approche métier et une expertise
          technique, dans la région IDF. Notre démarche créative avec des méthodes utiles nous permet de faire du conseil
         informatique et de donner à notre client la meilleure réponse.
		</p>
		
		<p>
        	<img src="imgs/eaht.gif" alt="le monde qui tourne" />
        	NOTRE OFFRE: <br />
        	Nos équipes pratiquent une mise à jour systématique pour suivre l'évolution des technologies.<br/>
			Notre valeur ajoutée repose sur une offre sans cesse adaptée et renouvelée pour anticiper et accompagner
			 les évolutions de nos clients partout où ils se trouvent dès lors qu’elles concernent l’une des trois dimensions :
			<br />
			<br />
				<span id="med">- Les métiers<br /></span>
				<span id="med">- Les organisations<br /></span>
				<span id="med">- Les systèmes d’informations<br /></span>
				
				
				
			
		</p>
        </section>
        
</div>



