<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="description" content="Le site de SNTP Capitalisation">
<title>Conseil informatique</title>
<link rel="shortcut icon" type="image/x-icon" href="imgs/mafav.ico" />
<link rel="stylesheet" type="text/css" href="styles/footer.css" /> 
<link rel="stylesheet" type="text/css" href="styles/header.css" /> 
<link rel="stylesheet" type="text/css" href="styles/menu.css" />
<link rel="stylesheet" type="text/css" href="styles/corps.css" />
<link rel="stylesheet" type="text/css" href="styles/body.css" /> 
<link rel="stylesheet" type="text/css" href="styles/loginForm.css" /> 
<link rel="stylesheet" type="text/css" href="styles/services.css" />
<link rel="stylesheet" type="text/css" href="styles/solutions.css" />
</head>
<body>
	<div id="main">
