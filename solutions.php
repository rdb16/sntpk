<?php
/*
 * Created on 10 juil. 2015
 * Created by rdb@sntpk.com
 *
 */
include_once 'includes/tophtml.php';
include_once 'includes/header.php';
include_once 'includes/menu.php';
?>
<body>
<div id="solutions">
<h1>Les solutions proposées sont liées à nos domaines de compétence :</h1>
<h2> Migration de bases de données</h2>
	<p>
		Export de vos anciennes bases de données au format texte, structures et datas, changement de l'encodage pour UTF-8<br/>
		Transformation de ces exports avec scripts SED ou AWK<br/>
		Enrichissement de la base via d'autres sources de données
	</p>
<h2> Capture de documents papier</h2>
	<p>
		Analyse de vos fonds documentaires.<br/>
		Mise en place de solutions de numérisation adaptées.
	</p>
<h2> GED indexation, OCR, Importation</h2>
	<p>
		Définition du choix de l'indexation avec les métiers.<br/>
		Choix des logiciels d'OCR par masque ou par "Full text"<br />
		Importations des index et images dans des logiciels de GED du commerce.<br />
		Choix et implémentation de solution ECM existante sur le marché
	</p>
<h2> Signature électronique</h2>
	<p>
		Choix d'une signature enveloppante, enveloppée, embarquée.<br/>
		Installation d'une PKI interne en Opensource.<br/>
		Request for signature auprès d'une AC.<br />
		Gestion des certificats<br />
		Signature automatisée d'un batch de fichiers

	</p>
<h2> Facturation électronique EDI ou PDF signé</h2>
	<p>
		Capture de vos fichiers sortis de votre progiciel de comptabilité<br/>
		Validation des informations contenues et conformité à la réglementation<br/>
		Transformation vers des formats xml, D96 A, PDF <br/>
		Signature si nécessaire<br/>
		Archivage légal en liaison avec Cecurity.com ( coffre électronique)<br/>
		Protocole de routage avec AR ( AS2 FTPS SFTP)<br/>
		Gestion et reprise des erreurs.

	</p>







</div>

</body>

<?php
	 include("includes/footer.php");
?>

</body>
</html>