




<?php
/*
 * Created on 10 juil. 2015
 * Created by rdb@sntpk.com
 * 
 */
 include_once 'includes/tophtml.php';
 include_once 'includes/header.php';
 include_once 'includes/menu.php';
?>
<body>
<div id="services">
<h1>les services proposés sont les suivants :</h1>
<ul><h3>
	<li> Installation d'un serveur Debian </li>
	<li> Installation de clients Debian, Ubuntu, Mint, .... </li>
	<li> Administration des systèmes Linux </li>
	<li> Installation d'un firewall du type ShoreWall ou IP-Tables sous linux</li>
	<li> Installation et paramétrage d'un serveur sécurisé d'emails sous linux ( PostFix)</li>
	<li> Installation et paramétrage Apache, sécurisation du serveur dont TLS</li>
	<li> Installation et paramétrage du container Tomcat (v > 7.0) </li>
	<li> Installation et paramétrage du container Glassfish ( v >4.0)</li>
	<li>Bases de données :</li>
	<ul><h4>
		<li>  Création du schéma avec vos règles métiers</li>
		<li>  Définition de la cardinalité entre les champs de différentes tables</li>
		<li>  Préparation des Queries pré-formatés et bench-tests sur la base</li>
		<li>  Analyse des benchs et optimisation des index, des instructions SQL</li>
		<li>  Paramétrages Mysql InoDB , entrelacement UTF-8</li>
		<li>  Création de vos vues, des procédures stockées, ...</li>
		<li>  paramétrage Postgres</li>
		</h4>
	</ul>
	<li> Réalisation de vos feuilles de style en CSS-3 pour HTML5 </li>
	<li> Réalisation de sites web spécialisés couplés à des bases de données</li>
	<li> Développement particuliers en java, bash, python, php5, autres</li>
	<li> Formation aux logiciels libres</li>
	</h3>
</ul>
</div>

<?php
	 include("includes/footer.php");
?>

</body>
</html>
